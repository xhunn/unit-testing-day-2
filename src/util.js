const factorial = (n) => {
    if (n === 0 || n === 1) {
        return 1
    } else {
        return n * factorial(n-1)
    }
}

const divisibleBySeven = (number) => {
    if (number === 0){
        return false
    } else if (number % 7 === 0) {
        return true
    } else {
        return false
    }
}

const divisibleByFive = (number) => {
    if (number === 0){
        return false
    } else if (number % 5 === 0) {
        return true
    } else {
        return false
    } 
}

module.exports = {
    factorial: factorial,
    divisibleByFive: divisibleByFive,
    divisibleBySeven: divisibleBySeven
}