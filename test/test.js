const { factorial, divisibleByFive, divisibleBySeven } = require('../src/util.js')
const { expect, assert } = require('chai');

describe('Test Factorials', () => {
    it('Test that 5! is 120', () => {
        const product = factorial(5);
        //expect(product).to.equal(120); //Alternative Syntax
        assert.equal(product, 120)
    });
    
    it('Test that 1! is 1', () => {
        const product = factorial(1)
        assert.equal(product, 1);
    })
    
    it('Test that 0! is 1', () => {
        const product = factorial(0)
        assert.equal(product, 1);
    })

    it('Test that 4! is 24', () => {
        const product = factorial(4)
        assert.equal(product, 24);
    })

    it('Test that 10! is 3628800', () => {
        const product = factorial(10)
        assert.equal(product, 3628800);
    })

    
})

describe('Check Divisibility', () => {
    it('Test that 105 is divisible by 5', () => {
        const quotient = divisibleByFive(105)
        assert.equal(quotient, true);
    })
    it('Test that 14 is divisible by 7', () => {
        const quotient = divisibleBySeven(14)
        assert.equal(quotient, true);
    })
    it('Test that 0 is divisible by 5 and 7', () => {
        const quotientOne = divisibleByFive(0)
        const quotientTwo = divisibleBySeven(0)
        let quotient;
        if (quotientOne && quotientTwo) {
            quotient = false;
        } else {
            quotient = true;
        }
        assert.equal(quotient, true);
    })
    it('Test that 22 is NOT divisible by 5 and 7', () => {
        const quotientOne = divisibleByFive(22)
        const quotientTwo = divisibleBySeven(22)
        let quotient;
        if (!quotientOne && !quotientTwo) {
            quotient = true;
        } else {
            quotient = false;
        }
        assert.equal(quotient, true);
    })

    
})